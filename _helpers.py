import os


def find_dirs_with_file(fn, search_path):
    """Find (nested) directories containing a file with a particular name, `fn`."""
    filename = fn.lower()
    tfiles = list()
    for dirpath, dirnames, filenames in os.walk(search_path):
        filenames = [f.lower() for f in filenames]
        if filename in filenames:
            tfiles.append(dirpath)
    return tfiles


def file_len(fname):
    """Calculate file length"""
    i = 0
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def split_path(path):
    """Split path into component parts"""
    folders = []
    while 1:
        path, folder = os.path.split(path)
        if folder != "":
            folders.append(folder)
        else:
            if path != "":
                folders.append(path)
            break
    folders.reverse()
    return folders
