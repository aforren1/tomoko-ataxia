import glob
import multiprocessing as mp
import os
import re
import warnings
from functools import partial
from timeit import default_timer
import numpy as np
import pandas as pd
from string import digits

remove_digits = str.maketrans('', '', digits)

from _helpers import find_dirs_with_file, file_len, split_path
# python3 please, and relies on numpy/pandas
# TODO: check 552-07, day 3 -- I *think* the aff/unaff labels were switched
# [site == '552' & subject == '07' & day == '003', task := ifelse(task == 'aff', 'unaff', 'aff')]


def find_good_subdirs(inpath):
    # get all directories with tfile
    tfile_dirs = find_dirs_with_file('tFile.tgt', inpath)
    # nix "Meas" dirs (not used AFAIK)
    tfile_dirs = [tf for tf in tfile_dirs if 'meas' not in tf.lower()]
    # check tFile health--should be at least some length,
    # and/or have some data
    tfile_lengths = [file_len(tf) for tf in [os.path.join(
        xx, 'tFile.tgt') for xx in tfile_dirs]]
    tfile_bad_length = [idx for idx, x in enumerate(tfile_lengths) if x < 2]
    for idx in sorted(tfile_bad_length, reverse=True):
        warnings.warn('tFile at %s is of improper length, so ignoring.' %
                      tfile_dirs[idx])
        del tfile_dirs[idx]
    return tfile_dirs


def check_duplicate_trialnames(path):
    datfiles = glob.glob(os.path.join(path, '*.dat'))  # list all dat in dir
    datfiles = [os.path.basename(i) for i in datfiles]
    # get the three numbers between two underscores
    datfiles = [re.search('_(.*)_', s).group(1)
                for s in datfiles if 'trial' in s.lower()]
    return len(datfiles) != len(set(datfiles))


def dir_to_igor(x, in_path, out_path):
    """Operates per-directory.
    Check for improper timestamps/lengths,
    missing "actual" movement.
    """
    # should be ~80 for a whole block (but could be less if early stop)
    datfiles = glob.glob(os.path.join(x, 'Trial*.dat'))
    for trial_idx, i in enumerate(datfiles):
        # import
        # cross fingers that everyone used underscores
        info = {}  # things that go into header of data file
        split_folds = split_path(i)
        info['trial_number'] = split_folds[-1].split('_')[1]
        info['subj_id'] = split_folds[-3]
        info['exp_type'] = split_folds[-2]
        try:
            keyvals, tbl = read_trial(i)
        except:
            warnings.warn('Malformed data in %s' % i)
            continue
        info.update(keyvals)
        # check length
        if not any(tbl['TargetX'] > -90):
            warnings.warn('Target never shown in %s' % i)
            continue
        # check timestamps
        time_col_name = [rr for rr in tbl.columns if 'time' in rr.lower()][0]
        diffs = np.diff(tbl[time_col_name][::4])
        if sum(diffs < 1e-6) > 10:
            # three sources (AFAIK):
            # 1. Overflow (e.g. 550-03_090_A)
            # 2. Weird thing (e.g. 551-2_000_A\Trial_000)
            # 3. No resolution (e.g. 552-03_003_A)
            warnings.warn('Time not great in %s' % i)
            continue
        for rr in range(4):
            tmp = tbl[tbl['Device_Num'] == rr+1]['HandX'] == 0.0
            if sum(tmp) > 20:
                warnings.warn('Sensor %i in %s might be weird?' % (rr+1, i))
                continue
        # now fill out the files
        # chop off the time before the target is visible (never used, AFAIK)
        idx = tbl[tbl['TargetX'].gt(-90)].index[0]
        tbl = tbl[idx:]
        target_x, target_y = tbl[['TargetX', 'TargetY']].iloc[0]
        start_x, start_y = tbl[['StartX', 'StartY']].iloc[0]
        info['angle'] = np.arctan2(
            target_y - start_y, target_x - start_x) * 180 / np.pi
        info.update({'target_x': target_x, 'target_y': target_y,
                     'start_x': start_x, 'start_y': start_y})
        # correct timestamp? only if int-based, but if float don't try (probably already secs)
        if tbl[time_col_name].dtype == 'int64':
            tbl[time_col_name] /= 1000.0
        # we need to go from long to wide
        out_tab = pd.DataFrame(data=tbl[time_col_name][::4])
        # first, fill in columns we won't use (we had four sensors: two per hand, and
        # two per proximal arm (so nothing corresponding strictly to elbow, shoulder))
        unused_cols = ['X1', 'Y1', 'Z1', 'X2', 'Y2', 'Z2', 'AZ1', 'EL1', 'RL1', 'AZ2',
                       'EL2', 'RL2', 'shx', 'shy', 'shz', 'elbx', 'elby', 'elbz']
        out_tab = out_tab.assign(**dict.fromkeys(unused_cols, 0.0))
        # now, use a heuristic to decide the "active" hand
        res = split_folds[-2].translate(remove_digits)
        nm = res[-1]
        if nm.lower() == 'r':
            hand_dev = 3
            sh_dev = 4
        else:
            hand_dev = 1
            sh_dev = 2
        out_tab['fingx'] = tbl[(tbl['Device_Num'] == hand_dev)]['HandX'].values
        out_tab['fingy'] = tbl[(tbl['Device_Num'] == hand_dev)]['HandY'].values
        out_tab['fingz'] = tbl[(tbl['Device_Num'] == hand_dev)]['HandZ'].values
        out_tab['shx'] = tbl[(tbl['Device_Num'] == sh_dev)]['HandX'].values
        out_tab['shy'] = tbl[(tbl['Device_Num'] == sh_dev)]['HandY'].values
        out_tab['shz'] = tbl[(tbl['Device_Num'] == sh_dev)]['HandZ'].values
        out_tab['X1'] = tbl[(tbl['Device_Num'] == 1)]['HandX'].values
        out_tab['Y1'] = tbl[(tbl['Device_Num'] == 1)]['HandY'].values
        out_tab['Z1'] = tbl[(tbl['Device_Num'] == 1)]['HandZ'].values
        out_tab['X2'] = tbl[(tbl['Device_Num'] == 3)]['HandX'].values
        out_tab['Y2'] = tbl[(tbl['Device_Num'] == 3)]['HandY'].values
        out_tab['Z2'] = tbl[(tbl['Device_Num'] == 3)]['HandZ'].values

        out_tab = out_tab.rename(index=str, columns={time_col_name: 'sec'})
        info['hand'] = nm  # save the guess, for sanity check
        # write header (from info)
        out_name = out_path + i[len(in_path):]
        # check if directories exist
        tmpdir = os.path.dirname(out_name)
        try:
            os.makedirs(tmpdir)
        except OSError:
            # if directory is already created, don't make it
            pass
        # clobber file if it exists (risky, I suppose?)
        with open(out_name, 'w+') as f:
            f.write('HEADER:\n')
            f.write('Filename\t' + os.path.basename(out_name) + '\n')
            f.write('Conditions\t' + info['exp_type'] + '\n')
            f.write('Date\t' + info['Date'] + '\n')
            f.write('Time\t' + info['Time'] + '\n')
            f.write('Columns\t' + str(out_tab.shape[1]) + '\n')
            f.write('Lines\t' + str(out_tab.shape[0]) + '\n')
            f.write('StartY\t' + str(info['start_y']) + '\n')
            f.write('StartX\t' + str(info['start_x']) + '\n')
            f.write('EndX\t' + str(info['target_x']) + '\n')
            f.write('EndY\t' + str(info['target_y']) + '\n')
            f.write('SubjName\t' + info['subj_id'] + '\n')
            f.write('sampleBin\t' +
                    str(1/(float(info['Sampling_Rate']))) + '\n')
            f.write('AcqSoftw\t' + info['Tracker'] + 'KinereachCpp\n')
            f.write('StartRadius\t0.01\n')  # in meters
            f.write('popTarRadius\t0.015\n')  # in meters
            f.write('direction\t' + str(info['angle']) + '\n')
            f.write('hand\t' + info['hand'] + '\n')
            f.write('******************\n')
            f.write('Names:\t\n')
            f.write('\t'.join(out_tab.columns) + '\n')  # header for data
            f.write('DATA:\t\n')
            out_tab.to_csv(f, sep='\t', header=False, index=False)
    # do the other settings files (maybe not required?)


def read_trial(filename):
    with open(filename, 'r') as f:
        header_keyvals = {}
        i = 0
        # parse header first
        for line in f:
            if line == '--\n':
                break
            name, var = line.partition(' ')[::2]
            name = name.replace(':', '')
            var = var.replace('\n', '')
            header_keyvals[name] = var
            i += 1
        #
        for line in f:
            line = line.replace(' \n', '')
            header = line.split(' ')
            break
        i += 3
    # read the bulk of the data
    tbl = pd.read_table(filename, sep=' ', header=None, skiprows=i)
    tbl.columns = header
    return header_keyvals, tbl


def convert_to_igor(in_path, out_path):
    """in_path: top-level dir for data
       out_path: top-level dir where new data will land
    """
    # find tFile.tgts
    good_subdirs = find_good_subdirs(in_path)
    # good_subdirs contains all directories with a tFile.tgt file.
    # next, check for (partial) duplicates of filenames
    # e.g. if there's two Trial_003_xxx
    # because less trivial to tell which set is the "correct" one
    for i in good_subdirs:
        if check_duplicate_trialnames(i):
            warnings.warn('Duplicated trial names in %s, so ignoring.' % i)
            good_subdirs.remove(i)

    # now we'll start converting files. There are two more situations
    # where we'll skip the file and/or directory:
    #   1. If there was no data before the "start" of the trial (experiment quit pre-stimulus)
    #   2. Bad timestamps (just in UZ data, AFAIK).
    # This will be parallelized, as well. We'll do per-block parallelization
    # (vs per-trial parallelization), so we don't have to keep spinning up new
    # python instances
    with mp.Pool(mp.cpu_count() - 1) as p:
        proc_file = partial(dir_to_igor, in_path=in_path, out_path=out_path)
        p.map(proc_file, (i for i in good_subdirs))


if __name__ == '__main__':
    t0 = default_timer()
    in_path = 'C:\\Users\\aforrence\\Documents\\shared_docs\\blam\\tomoko-ataxia\\AtaxiaData'
    out_path = in_path + '_igor'
    convert_to_igor(in_path, out_path)
    print('Total duration: ' + str(default_timer() - t0))
